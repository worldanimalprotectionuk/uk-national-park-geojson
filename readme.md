#UK National Park Boundaries GeoJSON
Originally compiled from [OpenStreetMap](http://www.openstreetmap.org) using [Overpass Turbo](http://overpass-turbo.eu) for use in the World Animal Protection [UK Wildlife Crime Map](http://www.worldanimalprotection.org.uk/uk-wildlife-crime-map).

##License
This data is made available under the Open Database License: [http://opendatacommons.org/licenses/odbl/1.0/](http://opendatacommons.org/licenses/odbl/1.0/). Any rights in individual contents of the database are licensed under the Database Contents License: [http://opendatacommons.org/licenses/dbcl/1.0/](http://opendatacommons.org/licenses/dbcl/1.0/) - See more at: [http://opendatacommons.org/licenses/odbl](http://opendatacommons.org/licenses/odbl)
